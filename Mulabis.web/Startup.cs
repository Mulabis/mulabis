﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mulabis.web.Startup))]
namespace Mulabis.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
